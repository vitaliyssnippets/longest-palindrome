﻿using System;

namespace LongestPalindrome
{
    class Program//Author: Vitaliy Karyuk
    {

        /// <summary>
        /// The entry point for the program to find the longest palindrom. Enter the input during the prompt.
        /// </summary>
        /// <param name="args">args are not used.</param>
        static void Main(string[] args)
        {
            Console.WriteLine("===Please enter an input sequence and the program will find the longest palindrome:");
            //Read input string
            string sequence = Console.ReadLine();
            //Display result
            Console.WriteLine("===The longest palindrome in the string is:\n" + GetLongestPalindrome(sequence));
            //Wait to exit
            Console.WriteLine("===Press Enter to exit.");
            Console.ReadLine();
        }

        /// <summary>
        /// Finds the longest palindrome string in a given string.
        /// </summary>
        /// <param name="str">The string str to check for palindromes.</param>
        /// <returns>Returns the longest palindrome string found.</returns>
        static string GetLongestPalindrome(string str)
        {
            if (string.IsNullOrEmpty(str)) return "";
            string longestPalindrome = "";
            int longestPalindromeLength = 0;
            int n = str.Length;
            for(int i = n - 1; i >= 0; i--)
            {//For each character in the string
                //Expand on the strings with odd centers and even centers
                string odd = ExpandPalindrome(str, i, i);
                string even = ExpandPalindrome(str, i, i + 1);
                if (odd.Length > longestPalindromeLength)
                {
                    longestPalindrome = odd;
                    longestPalindromeLength = longestPalindrome.Length;
                }
                if (even.Length > longestPalindromeLength)
                {
                    longestPalindrome = even;
                    longestPalindromeLength = longestPalindrome.Length;
                }
            }//Return the first longest palindrome found
            return longestPalindrome;
        }

        /// <summary>
        /// Determines the longest palindrome in a given string starting at two given indexes
        /// and expanding left and right from those indexes respectively.
        /// </summary>
        /// <param name="str">The string str to look for palindromes.</param>
        /// <param name="left">The left index to start expanding to the left from.</param>
        /// <param name="right">The right index to start expanding to the right from.</param>
        /// <returns>Returns the longest palindrome found as a string.</returns>
        static string ExpandPalindrome(String str, int left, int right)
        {
            int n = str.Length;
            string lastPalindrome = "";
            while (left >= 0 && right <= n - 1)
            {//While we have not yet reacher either end of the string
                int length = right - left + 1;
                //Create a substring from the left index to the right index
                string testStr = str.Substring(left, length);
                if (IsPalindrome(testStr))
                {//If the substring is a palindrome, then save it
                    lastPalindrome = testStr;
                    //Expand the search one character to the left, and one character to the right
                    left--;
                    right++;
                }
                else break;//Otherwise if we have not found a palindrome, leave loop
            }
            return lastPalindrome;//Return the last palindrome found, which is also the longest
        }

        /// <summary>
        /// Determines whether a given string is a palindrome.
        /// </summary>
        /// <param name="str">The string str to check for being a palindrome.</param>
        /// <returns>Returns true if str is a palindrome, false if not.</returns>
        static bool IsPalindrome(string str)
        {
            for(int i = 0; i < str.Length; i++)//Loop over the whole string
            {
                //Check that the string reads the same backwards by comparing characters,
                //if opposite characters dont match then str is not a palindrome
                if (str[i] != str[str.Length - 1 - i]) return false;
            }
            return true;//Otherwise it is a palindrome
        }


    }
}
