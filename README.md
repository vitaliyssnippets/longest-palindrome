Longest Palindrome is a program solving the following challange problem: A palindrome is a string that reads the same forwards and in reverse. Given a string of any length, determine the palindrome with the longest length within the string. i.e. In the string "TSTANABANASTR" the longest palindrome is "ANABANA".

To run, execute /LongestPalindrome/bin/Debug/LongestPalindrome.exe You require the .NET Framework installed on your machine.

Note: The important code is in /LongestPalindrome/Program.cs